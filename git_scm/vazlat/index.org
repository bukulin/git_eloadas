* Mi az a verziokoveto rendszer? (VCS, RCS, SCM, ...)
** Mit varunk el tole?
   - Teljes feljesztesi tortenet tarolasat
   - A fejlesztesi tortenetben minden verzio egyedileg azonosithato legyen. (Szam, betu, SHA-1, ...)
   - Nyomon lehessen kovetni, hogy ki, mikor, milyen valtozast hajtott vegre
   - Egyideju modositasok tamogatasa, osszefesules (merge) alapon.
   - Fogalmi szinten ismert legyen a pillanatfelvetel (tag) es a fejlesztesi ag (branch)
   - Ellenorizni lehessen, hogy egy adott allapot megfelel-e a szerzoje altal elkepzeltnek
   - Egyszeru es gyors hasznalat.
   - Fejlesztoi kornyezetbe integraltsag.
** Letezo megoldasok
*** Archaikus
    - Masolas
    - rcs
    - Verziozo fajlrendszer
      - Files-11 (OpenVMS)
      - ext3cow (Linux)
      - LMFS (Symbolics LISP Machine File System)
      - Time Machine (OS X)
*** Hagyomanyos, kozpontositott modell
    - CVS
    - SVN
    - Perforce
*** Elosztott verziokezelo rendszerek (DVCS) 
    - git
    - mercurial
    - bazaar
    - (monotone)
    - darcs
    - GNU arch
* Miert git?
** Tervezeskor figyelembe vett fo tulajdonsagok
*** Fejlesztesi agak hasznalata legyen konnyu
    - Gyors es kenyelmes kontextus (fejlesztesi ag) valtas
    - A fejlesztesi agak szerepei tisztazottak legyenek
    - A fejlesztesi metodusok minel szelesebb kore _tamogatott_ legyen
    - "Ingyenes" kiserletezes, fejlesztesi agak letrehozasa nem kerul semmibe.
*** Legyen gyors es kicsi
    - Mar a fejlesztes elejetol fogva nagy kodbazisokat kezelnek benne (linux kernel)
    - Az SVN-nel ket-harom nagysagrenddel gyorsabb
    - A tarolt fajlok merete a leheto legkisebb legyen
*** Elosztott
    - Minden peldany tartalmazza a teljes fejlesztesi tortenetet.
    - Szokasos fejlesztesi folyamatok kezelese
      - SVN szeru
      - Integracio felelos
      - Vezerezredes - hadnagy - kozkatona
    - Offline lehet hasznalni, a fejlesztes nem akad meg szerver hiba miatt.
*** Adatintegritas biztositasa
    - Minden commit egy SHA-1 ellenorzo oszzeggel rendelkezik, amibe beleszami:
      - Szulo commitok SHA-1 ellenorzo osszege
      - Aktualis BLOB ellenorzo osszege
    - Ha a fejlesztesi tortenet barhol megvaltozik, az SHA-1 ellenorzo osszeg is megvaltozik
    - A commitok elsodleges azonositoja a (roviditett) SHA-1 osszeg
*** Programozoktol programozoknak
    - Fejlesztesekor a valos fejlesztesi szituaciokat igyekeztek
      megoldani, nem mukodeskeptelen fejlesztesi modelleket
      kialakitani.
    - Van lehetoseg a hibak javitasara (majdnem mindenre)
** Trendek kovetese
Jelenleg is sokan hasznaljak, ez az uj de-facto szabvany. Par emlitesre melto pelda:
   - linux kernel
   - google
   - facebook
   - Microsoft
   - twitter
   - LinkedIn
   - Android
   - Ruby on Rails
* git beallitasa
** 0. lepes: felejtsunk el mindent!
** Koncepcio
*** Pillanatkepek, nem valtozasok
Minden kommit a fajlok egy aktualis allapotat tarolja, nem a
fajlszintu valtozasok halmazat.
*** git depo, mint mini fajlrendszer
Erdemes ugy tekinteni a lokalis git depo-ra, mint egy mini
fajlrendszerre, amit egyszeruen es hatekonyan tudunk kezelni.
*** Elosztott verziokovetes
**** Majdnem minden muvelet lokalis fajlokra tamaszkodik
     - Ha barmilyen halozati vagy egyeb szolgaltatas problema van, nem all meg az elet.
     - Mivel szinte minden lokalisan zajlik, nem kell halozati kommunikaciora varni, igy gyors.
*** Harom szintu valtozas kovetes
**** Szokasosan ket szint van:
     - Lokalis valtozas
     - Committolt valtozas
**** Harom elkulonulo szint
     - Lokalis valtozas
     - Committolni kivant valtozasok halmaza
     - Committolt valtozas
** Identitas beallitasa
#+BEGIN_SRC bash
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
#+END_SRC
Mindig keznel a segitseg:
#+BEGIN_SRC bash
$ git help <command>
$ git <command> --help
#+END_SRC
* git alap muveletek
** Lokalis konyvtarrendszer verziokezelese gittel
#+BEGIN_SRC bash
$ cd <target directory>
$ git init
#+END_SRC
** Csatlakozas egy mar meglevo projekthez
#+BEGIN_SRC bash
$ git clone <repository>
#+END_SRC
** Valtozasok kovetese
*** Aktualis allapot lekerdezese
#+BEGIN_SRC bash
$ git status
#+END_SRC
*** Uj fajl
=main.c=:
#+NAME: main.c
#+BEGIN_SRC C
int main(void)
{
	return 0;
}
#+END_SRC
*** Stage
#+BEGIN_SRC bash
$ git add main.c
#+END_SRC
*** Commit
#+BEGIN_SRC bash
$ git commit -m "Dummy main function"
#+END_SRC
A javaslat szerint a git commit formatuma igy nez ki:
#+BEGIN_SRC text
Rovid osszefoglalo

Reszletes leiras arrol, hogy a valtozas miert tortent,
nem arrol, hogy mi tortent
(Esetleges hibajegy megemlitese)
#+END_SRC
*** Valtoztatas a kodon
=main.c=:
#+NAME: main.c
#+BEGIN_SRC C
#include <stdio.h>

int main(void)
{
	printf("Hello World!\n");
	return 0;
}
#+END_SRC
*** Stage
#+BEGIN_SRC bash
$ git add main.c
#+END_SRC
*** Commit
#+BEGIN_SRC bash
$ git commit -m "Communication improvement"
#+END_SRC
*** Fajlok atnevezese es eltavolitasa
#+BEGIN_SRC bash
$ git mv <source> <destination>
$ git rm <target>
#+END_SRC
*** Attekintes
A fajlok negy fele allapotot vehetnek fel:
    - Untracked
    - Unmodified
    - Modified
    - Staged
Az allapotok kozott megfelelo git parancsokkal lehet leptetni a
fajlokat.
** Fejlesztesi tortenet
*** Minden commit lekerdezese innentol visszafele:
#+BEGIN_SRC bash
$ git log
#+END_SRC
Szamos hasznos opcioval rendelkezik. Pl:
 - graf rajzolas
 - minden fejlesztesi ag mutatasa
 - sajat formatum szerinti kiiratas
 - limitacio idoben vagy adott committol
*** Kereses a fejlesztesi tortenetben:
#+BEGIN_SRC bash
$ git log -S "Hello World"
$ git log -G "[hH]ello.*World"
#+END_SRC
** Branch-ek hasznalata
Minden branch egy _pointer_ a fejlesztesi tortenet fajaban.
Az aktualis mutatora mutato a HEAD.
Abrak es reszletes leiras [[http://git-scm.com/book/en/Git-Branching-What-a-Branch-Is][itt]] talalhatok.
*** Uj branch keszitese
#+BEGIN_SRC bash
$ git branch new_feature_branch <base>
$ git checkout new_feature_branch
#+END_SRC
Vagy rovidebben:
#+BEGIN_SRC bash
$ git checkout -b new_feature_branch
#+END_SRC
*** Branch-ek kezelese
Listazas
#+BEGIN_SRC bash
$ git branch
#+END_SRC
Valtas az agak kozott
#+BEGIN_SRC bash
$ git checkout <branch name>
#+END_SRC
*** Branch-ek osszefesulese (merge)
#+BEGIN_SRC bash
$ git checkout master
$ git merge new_feature_branch
#+END_SRC
Kulonbseg van a tenyleges merge es a fast-forward merge kozott!
*** Segitseg, sok branch-em van!
Kit fesultem mar be?
#+BEGIN_SRC bash
$ git branch --merged
#+END_SRC

Kit nem fesultem meg be?
#+BEGIN_SRC bash
$ git branch --no-merged
#+END_SRC

** A ket fele merge-elesi strategia
*** Merge
A merge biztositekot ad arra, hogy az osszefesules pontjaban a
fejlesztesi tortenet minden valtozast tartalmazni fog, amit a szulok
tartalmaznak. Eppen ezert megkoveteli, hogy fejlesztesi tortenetunk
preciz legyen. Ez utobbihoz a git sok segitseget biztosit.
*** Cherry-pick
Nem biztositott, hogy minden valtoztatasunk jelen lesz a fejlesztes
egy adott pontjaban, ugyanakkor nem kell preciz fejlesztesi tortenetet
fenntartanunk. Mindenkepp kod felulvizsgalat szukseges.
* Tavoli kodbazisok kezelese
** Tavoli tarolo csatolasa
#+BEGIN_SRC bash
$ # git remote add <repo name> <repo url>
$ git remote add origin git://github.com/bukuilin/git_eloadas.git
#+END_SRC
** Tavoli tarolok frissitese
#+BEGIN_SRC bash
$ git remote update
#+END_SRC
** Tavoli branch-ek
#+BEGIN_SRC bash
$ git branch -r		# tavoli branch-ek listazasa
$ git branch -a		# minden branch listazasa
#+END_SRC
** Tavoli valtozasok alkalmazasa helyben
#+BEGIN_SRC bash
$ # git fetch <repo nev> <branch nev>
$ git fetch origin master
$ git merge FETCH_HEAD
#+END_SRC
Vagy rovidebben
#+BEGIN_SRC bash
$ git pull origin master
#+END_SRC
** Valtozasok publikalasa
#+BEGIN_SRC bash
$ # git push <repo nev> <lokalis branch nev>[:<tavoli branch nev>]
$ git push origin master:master
#+END_SRC
** Tavoli branch torlese
#+BEGIN_SRC bash
$ git push origin :<torlendo tavoli branch neve>
#+END_SRC
* git halado szinten
** Korabbi commit visszavonasa (egy uj committal)
Bar erre meglehetosen ritkan van szukseg, ez a modja:
#+BEGIN_SRC bash
$ git revert <commit SHA1>
#+END_SRC
** Kituntetett verziok keszitese (tag)
#+BEGIN_SRC bash
$ git tag v0.1
#+END_SRC
** Commitok ujrafuzese egy adott ponttol (rebase)
#+BEGIN_SRC bash
$ git checkout my_branch_to_rebase
$ git rebase my_base_branch
#+END_SRC
Abrak es reszletes leiras [[http://git-scm.com/book/en/Git-Branching-Rebasing][itt]].
** Hibakereses tartomanyfelezessel
** Fajlok figyelmen kivul hagyasa
Barmelyik konyvtarban elhelyezheto egy =.gitignore= fajl, amelynek
tartalma alapjan a git bizonyos fajlokat figyelmen kivul hagy.
* Hasznos eszkozok
** Valtozasok reszletes hozzaadasa az indexhez
#+BEGIN_SRC bash
$ git add -p
#+END_SRC
** Interaktiv index/stage kezeles
#+BEGIN_SRC bash
$ git add -i
#+END_SRC
** Elozo commit javitasa
#+BEGIN_SRC bash
$ git commit --amend
#+END_SRC
** Korabbi commit javitasa
#+BEGIN_SRC bash
$ git commit		# esegleg commit megjelolese fixup!-pal vagy squash!-al
$ git rebase -i <base>
#+END_SRC
** Eg a haz, mindenki dobjon el mindent!
A jelenlegi munkam meg nincs kesz, nem akarom committolni, viszont
valahova el kell tennem, hogy egy masik branch-el foglalkozzak.
#+BEGIN_SRC bash
$ git stash
#+END_SRC

Ha keszen vagyok visszaterni ehhez a munkahoz:
#+BEGIN_SRC bash
$ git stash appy
$ git stash drop
#+END_SRC
Vagy rovidebben
#+BEGIN_SRC bash
$ git stash pop
#+END_SRC

** Hibakereses gittel
Beepitett segitseg intervallum felezeses modszerrel torteno keresesre:
#+BEGIN_SRC bash
$ git bisect start
$ git bisect <good|bad>
$ git bisect reset
#+END_SRC
** Projekt - Alprojekt
*** Submodule
#+BEGIN_SRC bash
$ git submodule add
$ git submodule init
$ git submodule update
#+END_SRC
*** Subtree merge
Ennek meg utana kell jarni, de jobbnak itelik meg mint a submodule megkozelitest
* Tovabbi temak
** Fejlesztesi agak hasznalata
** Folytonos integracio
** Tesztvezerelt fejlesztes
** Automatizalt viselkedesvezerelt fejlesztes
* Referenciak
** git konyv
Ingyenes es jo!
http://git-scm.com/book
** Cheat sheet
http://git-scm.com/docs
https://training.github.com/kit/downloads/github-git-cheat-sheet.pdf
http://ndpsoftware.com/git-cheatsheet.html
** Egyeb dokumentacio
http://git-scm.com/doc/ext
* Fuggelek
** git hasznalata nem forraskod kezelesere
*** config karbantartas
*** stow
** git-annex
*** privat "felho" nagy meretu fajljaink tarolasara
** git integracioja fejlesztoeszkozokkel
*** emacs: magit
** GUI-k windows-ra
A git honlapjan szamos talalhato, en a SourceTree-t probaltam ki, az igeretesnek tunt.
** Porcelain vs plumbing
