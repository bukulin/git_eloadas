(add-to-list 'ispell-local-dictionary-alist '("hungarian-hunspell"
                                              "[[:alpha:]]"
                                              "[^[:alpha:]]"
                                              "[']"
                                              t
                                              ("-d" "hu_HU"); Dictionary file name
                                              nil
                                              utf-8))

(setq ispell-program-name "hunspell"          ; Use hunspell to correct mistakes
      ispell-dictionary   "hungarian-hunspell") ; Default dictionary to use
